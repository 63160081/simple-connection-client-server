import socket

def main():
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    host = '127.0.0.1'  # IP address ของ Server (ให้ใส่ IP address ของคอมพิวเตอร์ที่เราใช้งานอยู่)
    port = 12345       # Port ที่ Server ใช้ในการรอรับการเชื่อมต่อ

    client_socket.connect((host, port))

    number = 1
    while number <= 100:
        # รับตัวเลขจาก Server
        received_data = client_socket.recv(1024)
        received_number = int(received_data.decode())
        print("Server:", received_number)

        if received_number == 100:
            break

        # นำค่าที่ได้รับมาบวก 1 และส่งกลับไปยัง Server
        number = received_number + 1
        client_socket.send(str(number).encode())

    # ปิดการเชื่อมต่อกับ Server เมื่อค่าตัวเลขมีค่าเท่ากับ 100
    client_socket.close()

if __name__ == "__main__":
    main()
