import socket

def main():
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    host = '127.0.0.1'  # IP address ของ Server (ให้ใส่ IP address ของคอมพิวเตอร์ที่เราใช้งานอยู่)
    port = 12345       # Port ที่ Server ใช้ในการรอรับการเชื่อมต่อ

    server_socket.bind((host, port))
    server_socket.listen(5)

    print("Server is ready")

    client_socket, client_address = server_socket.accept()

    number = 1
    while number <= 100:
        # ส่งตัวเลขไปยัง Client
        client_socket.send(str(number).encode())

        # รับตัวเลขที่ Client ส่งกลับมา
        received_data = client_socket.recv(1024)
        received_number = int(received_data.decode())
        print("Client:", received_number)

        # นำค่าที่ได้รับมาบวก 1
        number = received_number + 1

    # ปิดการเชื่อมต่อกับ Client เมื่อค่าตัวเลขมีค่าเท่ากับ 100
    client_socket.close()
    server_socket.close()

if __name__ == "__main__":
    main()
